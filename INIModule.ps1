# Autor = "vitoxaya"
# Repository = "https://gitlab.com/vitoxa_ya/PSModules.git"
# License = "GNU General Public License (GPL) v3.0"

function Get-IniConfig($Path){
  $config = @{}
  if((Test-Path -Path "$Path" -PathType Leaf) -and ($Path).Contains('.ini')){
    Get-Content -Path "$Path" | ForEach-Object {
      if ($_ -match "^\[([^\]]+)\]$") {
          $section = $Matches[1]
          $config[$section] = @{}
      }
      elseif ($_ -match "^([^=]+)\s*=\s*(.*)$") {
          $key = $Matches[1]
          $value = $Matches[2]
          $config[$section][$key] = $value
      }
    }
  }
  return $config
}
